<?php
/**
 * Created by PhpStorm.
 * User: Колесников Денис
 * Date: 10.12.2017
 * Time: 16:40
 */

class User
{
    protected $name;

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * User constructor.
     * @param $name
     */
    public function __construct($name)
    {
        $this->name = $name;
    }


}